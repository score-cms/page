<?php

namespace Score\PageBundle\Controller;

use Score\BaseBundle\Services\AdjacencyArrayTreeManager;
use Score\BaseBundle\Services\AdjacencyTreeManager;
use Score\PageBundle\Services\PageManager;
use Score\PageBundle\Entity\Page;
use Score\CmsBundle\Entity\Menu;
use App\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PublicController extends AbstractController
{

    /**
     * @Route("/", name="default_homepage")
     * @Route("/{seoid}", name="default_subpage", requirements={"seoid"="^(?!login$|logout$|registration$|api/|user/|clanky/|file/|admin/).+"}, condition="!request.isXmlHttpRequest()")
     */
    public function subpageAction(PageManager $pageManager, $seoid = null)
    {
        $responseCode = 200;
        if ($seoid) {
            if ($seoid === "homepage") {
                return $this->redirectToRoute('default_homepage');
            }
            $page = $pageManager->getPageBySeoId($seoid);
            if (!$page) {
                $page = $pageManager->getPageBySeoId('error');
                $responseCode = 404;
            }
        } else {
            $page = $pageManager->getPageBySeoId('homepage');
        }

        return new Response($this->renderView('@ScorePage/Public/subpage.html.twig', [
            'page' => $page,
        ]), $responseCode);
    }


    public function mainMenuAction(AdjacencyArrayTreeManager $adjacencyArrayTreeManager, $options = [])
    {
        $em = $this->getDoctrine()->getManager();
        $pageRepo = $em->getRepository(Menu::class);
        $rootMenu = $pageRepo->findOneBy(['parentId' => 0]);
        $menuTree = $this->getMenuTree($adjacencyArrayTreeManager, $rootMenu->getId());

        $template = '@ScorePage/Public/mainMenu.html.twig';
        if (array_key_exists('template', $options)) {
            $template = $options['template'];
        }

        return $this->render($template, [
            'menuTree' => $menuTree,
            'requestUri' => $_SERVER['REQUEST_URI'],
        ]);
    }

    public function blockMenuAction(AdjacencyArrayTreeManager $adjacencyArrayTreeManager, $menuId)
    {
        $menuTree = $this->getMenuTree($adjacencyArrayTreeManager, $menuId);

        return $this->render('@ScorePage/Public/blockMenu.html.twig', [
            'menuTree' => $menuTree,
        ]);
    }

    private function getMenuTree($adjacencyArrayTreeManager, $menuId)
    {
        $em = $this->getDoctrine()->getManager();
        $treeManager = $adjacencyArrayTreeManager;
        $mainMenuItems = $em->getRepository(Menu::class)
            ->findBy(['rootId' => $menuId], ['lvl' => 'ASC', 'sortOrder' => 'ASC']);
        $treeManager->setCollection($mainMenuItems);
        return $treeManager->buildTree($menuId);
    }
}
