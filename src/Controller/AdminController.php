<?php

namespace Score\PageBundle\Controller;

use Score\BaseBundle\Services\AdjacencyArrayTreeManager;
use Score\BlockBundle\Services\CmsBlockManager;
use Score\CmsBundle\Entity\ChangesLog;
use Score\PageBundle\Entity\PageBlock;
use Score\PageBundle\Form\PageForm;
use Score\PageBundle\Entity\Page;
use Score\BlockBundle\Entity\Block;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Score\BaseBundle\Controller\AdminController as BaseAdminController;
use Score\PageBundle\Services\AdminPageManager;
use Score\PageBundle\Services\PageManager;
use Score\PageBundle\Repository\PageDatagrid;

class AdminController extends BaseAdminController
{

    /**
     * Data for datatable
     *
     * @Route("/admin/page/datatable", name="page_admin_datatable", methods={"GET"})
     *
     */
    public function datatableDataAction(SessionInterface $session, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $datagrid = new PageDatagrid($em, 'PageBundle:Page', $this->container->get('router'));
        $datagrid->setPagerLength(10);
        $datagrid->setPagerStart($request->get('start'));
        $datagrid->setOrderParams($request->get('order'));

        $data = $datagrid->getRecordsAsArray();

        $recordsTotal = $datagrid->getRecordsTotal();
        $recordsFiltered = $datagrid->getRecordsFiltered();

        $datagridData = array(
            'draw' => $this->getDatagridDraw($session),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data,
        );
        return $this->json($datagridData);
    }


    public function getRootPage()
    {
        $em = $this->getDoctrine()->getManager();
        $pageRepo = $em->getRepository(Page::class);
        $page = $pageRepo->findOneBy(['name' => 'root']);
        //$pageManager = $this->get('score.admin.page.manager');
        //$page = $pageManager->getPageById(1);
        return $page;
    }

    /**
     * @Route("/admin/page/list", name="score_cms_pages")
     */
    public function indexAction(AdminPageManager $pageManager)
    {
        $list = $pageManager->getPageParentChoices($this->getRootPage()->getId());

        return $this->render('@ScorePage/Admin/index.html.twig', array(
            'list' => $list
        ));
    }

    /**
     * @Route("/admin/page/subpages", name="score_cms_page_subpages")
     */
    public function subpagesAction()
    {

        $pageManager = $this->get('score.admin.page.manager');
        $parent = $pageManager->getPageById($this->get('request')->get('parent_id'));
        $list = $pageManager->getPagesByParent($parent);

        $data = array();
        $result = array();
        foreach ($list as $page) {
            $entityData = $pageManager->convertEntityToArray($page);
            $entityData['link'] = $this->generateUrl('score_cms_page_subpages', array('parent_id' => $page->getId()));
            $entityData['edit_link'] = $this->generateUrl('score_cms_page_edit', array('id' => $page->getId()));
            $entityData['delete_link'] = $this->generateUrl('score_cms_page_delete', array('id' => $page->getId()));
            $data[] = $entityData;
        }

        // create a JSON-response with a 200 status code

        $result['subpages'] = $data;
        $result['add_link'] = $this->generateUrl('score_cms_page_create', array('parent_id' => $this->get('request')->get('parent_id')));


        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    private function buildForm($page)
    {
        $multisite = [];
        foreach($page->getSiteItemPage() as $item)
        {
            $multisite[] = $item->getSite();
        }
        $page->setMultisite($multisite);
        $form = $this->createForm(PageForm::class, $page,[]);
        return $form;
    }

    /**
     * @Route("/admin/page/create", name="score_cms_page_create")
     */
    public function createAction(AdminPageManager $pageManager, Request $request)
    {
        //$pageManager = $this->get('score.admin.page.manager');
        //$blockManager = $this->get('score.cms.block.manager');
        $page = new Page();
        //$pageForm = new PageForm();
        $form = $this->buildForm($page);
        $em = $this->getDoctrine()->getManager();

        $parentPages = $pageManager->getPageParentChoices();

        $form->handleRequest($request);
        $activeSection = (null == $request->get('s')) ? 'page' : $request->get('s');
        if ($form->isSubmitted() && $form->isValid()) {
            if (null != $request->get('parent_page')) {
                $parentPage = $parentPages[$request->get('parent_page')];
            } else {
                $parentPage = $this->getRootPage($pageManager);
            }
            $page->setParentPage($parentPage);
            $page->setLvl($parentPage->getLvl() + 1);
            $em->persist($page);
            $pageManager->handleMultisite($page);
            //create default block
            $block = $em->getRepository(Block::class)->findOneBy(['type' => 'pageContent']);
            $pageBlock = new PageBlock();
            $pageBlock->setBlock($block);
            $pageBlock->setPage($page);
            $pageBlock->setPlace('center');
            $em->persist($pageBlock);


            $em->flush();


            return $this->redirectToRoute('score_cms_pages');
        }


        return $this->render('@ScorePage/Admin/create.html.twig', array(
            'form' => $form->createView(),
            'activeSection' => $activeSection,
            'parentPages' => $parentPages,
            'page' => $page,
            'domain' => $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'],
            'subpages' => [],
        ));
    }


    /**
     * @Route("/admin/page/edit", name="score_cms_page_edit")
     */
    public function editAction(Request $request, AdminPageManager $pageManager, AdjacencyArrayTreeManager $treeManager)
    {
        $em = $this->getDoctrine()->getManager();
        $pageRepo = $em->getRepository(Page::class);

        //$treeManager =  $this->get('score.manager.adjacency_tree');
        $treeManager->setRepository($pageRepo);

        $page = $pageManager->getPageById($request->get('id'));
        $form = $this->buildForm($page);

        $parentPages = $pageManager->getPageParentChoices();
        $pageBlocks = array();
        foreach ($page->getPageBlocks() as $pageBlock) {
            $pageBlocks[$pageBlock->getBlock()->getId()] = $pageBlock;
            //dump($pageBlock);
        }

        $subpages = $pageManager->getPagesByParent($page);
        //$breadcrumb = $treeManager->getObjectParentChain($page); //todo asi nav in top menu

        //$blocks =

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (null != $request->get('parent_page')) {

                $page->setParentPage($parentPages[$request->get('parent_page')]);
            }
            $pageManager->handleMultisite($page);

            $em = $this->getDoctrine()->getManager();
            $em->persist($page);
            $em->flush();

            $this->addFlash('score_cms_page_edit_success', 'Zmeny boli uložené');
            return $this->redirectToRoute('score_cms_page_edit', array('id' => $page->getId()));
        }

        //dump($pageManager->getAvailableBlocks($page));


        $activeSection = (null == $request->get('s')) ? 'page' : $request->get('s');

        $logs = $em->getRepository(ChangesLog::class)->findBy(["entity" => Page::class, "entityId" => $page->getId(), "field" => "content"], ["editedAt" => "DESC"], 15);

        return $this->render('@ScorePage/Admin/edit.html.twig', array(
            'form' => $form->createView(),
            'availableBlocks' => $em->getRepository(Block::class)->loadBlocksList(),
            'page' => $page,
            'pageBlocks' => $pageBlocks,
            'activeSection' => $activeSection,
            'domain' => $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'],
            'parentPages' => $parentPages,
            'subpages' => $subpages,
            'logs' => $logs
        ));
    }

    /**
     * @Route("/admin/page-preview/{id}", name="subpage_preview")
     */
    public function subpagePreviewAction(Request $request, PageManager $pageManager)
    {
        //$this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
        $page = $pageManager->getPageById($request->get('id'));
        return $this->render('@ScorePage/Subpage/page.html.twig', array(
            'page' => $page,
        ));
    }

    /**
     * @Route("/admin/page/reorder", name="score_cms_page_subpages_reorder")
     */
    public function reorderPagesAction()
    {
        $items = $this->get('request')->get('page');
        $pageManager = $this->get('score.admin.page.manager');
        $pageManager->reorderPages($items);

        $response = new Response(json_encode(array('status' => 'Success')));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/admin/page/block/edit", name="score_cms_page_edit_blocks")
     */
    public function editBlocksAction(PageManager $pageManager, CmsBlockManager $blockManager, Request $request)
    {
        //$pageManager = $this->get('score.admin.page.manager');
        //$blockManager = $this->get('score.cms.block.manager');
        $blocks = $request->get('page_blocks');

        //$blockPlaces  =$request->get('place');
        $page = $pageManager->getPageById($request->get('id'));
        $em = $this->getDoctrine()->getManager();

        //clear blocks
        $pageBlocks = $page->getPageBlocks();
        foreach ($pageBlocks as $pageBlock) {
            $em->remove($pageBlock);
        }
        $em->flush();


        if (null != $blocks) {
            foreach ($blocks as $placeCode => $placeBlocks) {
                foreach ($placeBlocks as $blockId) {
                    $block = $blockManager->getBlockById($blockId);
                    $pageBlock = new PageBlock();
                    $pageBlock->setBlock($block);
                    $pageBlock->setPage($page);
                    $pageBlock->setPlace($placeCode);
                    $em->persist($pageBlock);
                }


            }
        }
        $em->flush();
        $this->addFlash('score_cms_page_edit_success', 'Zmeny boli uložené');
        return $this->redirectToRoute('score_cms_page_edit', array('id' => $page->getId(), 's' => 'block'));
    }

    /**
     * @Route("/admin/page/block/reorder", name="score_cms_page_reorder_blocks")
     */
    public function reorderBlocksAction()
    {
        $items = $this->get('request')->get('block');
        $pageManager = $this->get('score.admin.page.manager');
        $page = $pageManager->getPageById($this->get('request')->get('page_id'));
        $em = $this->getDoctrine()->getManager();
        $pageBlocks = $page->getPageBlocks();
        foreach ($pageBlocks as $pageBlock) {
            //$page->removePageBlock($pageBlock);
            $sortOrder = array_search($pageBlock->getId(), $items);
            $pageBlock->setSortOrder($sortOrder);
            $em->persist($pageBlock);
        }
        $em->flush();


        $response = new Response(json_encode(array('status' => 'Success')));
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }

    /**
     * @Route("/admin/page/delete", name="score_cms_page_delete")
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $pageManager = $this->get('score.admin.page.manager');
        $page = $pageManager->getPageById($request->get('id'));

        $adjacency_tree_manager = $this->get('score.page.tree.manager');
        $childs = $adjacency_tree_manager->buildArray($page->getId());

        $childs = array_reverse($childs);

        foreach ($childs as $child) {
            $em->remove($child);
        }

        $em->remove($page);
        $em->flush();
        $this->addFlash('score_cms_page_delete_success', 'Stránka bola odstránená');
        return $this->redirectToRoute('score_cms_pages');
    }


}
