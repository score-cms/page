<?php
namespace Score\PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
class PageForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null,array('label' => 'score.page.edit.name'))
            ->add('title',null,array('label' => 'score.page.edit.title'))
            ->add('seo_id',null,array('label' => 'score.page.edit.seoId'))
            ->add('status',ChoiceType::class,array('label' => 'score.page.edit.status.label','choices' => array(
                '' => '',
                'score.page.edit.status.public' => 'public',
                'score.page.edit.status.hidden' => 'hidden'
            )))
            ->add('layout',ChoiceType::class,array('label' => 'score.page.edit.layout.label','choices' => array(
                '' => '',
                'score.page.edit.layout.MLCR' =>'menu-left-center-right',
                'score.page.edit.layout.MLC' => 'menu-left-center',
                'score.page.edit.layout.MC' => 'menu-center',
                'score.page.edit.layout.MCR' => 'menu-center-right',
            )))
            ->add('content',TextareaType::class,array('label' => 'score.page.edit.content'))
            ->add('multisite', EntityType::class, array(
                'class' => \Score\CmsBundle\Entity\Multisite\Site::class,
                    'multiple' => true,
                    'expanded' => true,
                    'choice_label' => 'name',
                    'label' => 'score.page.edit.multisite',
                    'required' => false
                ))
        ;
    }

    public function getName()
    {
        return 'page';
    }
}
