<?php

namespace Score\PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('subject', null, array('label' => 'contact.form.subject', 'required' => true,'constraints' => [new NotBlank()]))
                ->add('email', null, array('label' => 'contact.form.email', 'required' => true,'constraints' => [new NotBlank()]))
                ->add('body', TextareaType::class, array('label' => 'contact.form.body', 'required' => true,'constraints' => [new NotBlank()]))
        ;
    }

    public function getName()
    {
        return 'contact';
    }

}
