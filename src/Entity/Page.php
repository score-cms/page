<?php

namespace Score\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Score\BaseBundle\Entity\BaseEntity;
use Score\PageBundle\Entity\PageBlock;

/**
 * @ORM\Entity(repositoryClass="Score\PageBundle\Entity\PageRepository")
 * @ORM\Table(name="cms_page")
 * @ORM\HasLifecycleCallbacks()
 */
class Page extends BaseEntity {


    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $name;

    /**
     * @ORM\Column(name="title", type="string", length=200)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=200, nullable = true)
     */
    protected $linkName;

    /**
     * @ORM\ManyToOne(targetEntity="Page")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parentPage;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $lvl;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $sortOrder;

    /**
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    protected $seoId;

    /**
     * @ORM\Column(name="content", type="text", length=10000, nullable = true)
     */
    protected $content;

    /**
     * @ORM\Column(type="string", length=100, nullable = true)
     */
    protected $status;



    /**
     * @ORM\Column(type="string", nullable = true)
     */
    protected $lang;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $parentId;

    /**
     * @ORM\OneToMany(targetEntity="PageBlock", mappedBy="page")
     */
    private $pageBlocks;

    /**
     * @ORM\Column(name="config_data", type="text", length=10000, nullable = true)
     */
    protected $configData;

    /**
     * @ORM\Column(type="string", length=100, nullable = true)
     */
    protected $layout;

    /*//*
     * // //@--ORM\OneToMany(targetEntity="\KeywordBundle\Entity\KeywordItemPage", mappedBy="page")
     *///
    private $keywordItemPage;

    /**
     * @ORM\Column(type="string", length=2000, nullable = true)
     */
    protected $redirectUrl;

    /**
     * @ORM\Column(type="string", length=100, nullable = true)
     */
    protected $redirectWindow;

    /**
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    protected $icon;

    /**
     * @ORM\Column(type="string", length=2000, nullable = true)
     */
    protected $teaser;


     /**
     * @ORM\OneToMany(targetEntity="\Score\CmsBundle\Entity\Multisite\SiteItemPage", mappedBy="page",cascade={"persist","remove"})
     */
    private $siteItemPage = [];

    private $multisite;


    public function getKeywordItemPage()
    {
        return $this->keywordItemPage;
    }

    public function setKeywordItemPage($keywordItemPage)
    {
        $this->keywordItemPage = $keywordItemPage;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getLayout()
    {
        return $this->layout;
    }

    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Page
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set linkName
     *
     * @param string $linkName
     * @return Page
     */
    public function setLinkName($linkName)
    {
        $this->linkName = $linkName;

        return $this;
    }

    /**
     * Get linkName
     *
     * @return string
     */
    public function getLinkName()
    {
        return $this->linkName;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     * @return Page
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return Page
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set seoId
     *
     * @param string $seoId
     * @return Page
     */
    public function setSeoId($seoId)
    {
        $this->seoId = $seoId;

        return $this;
    }

    /**
     * Get seoId
     *
     * @return string
     */
    public function getSeoId()
    {
        return $this->seoId;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Page
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }



    /**
     * Set lang
     *
     * @param string $lang
     * @return Page
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set parentPage
     *
     * @param Page $parentPage
     * @return Page
     */
    public function setParentPage(Page $parentPage = null)
    {
        $this->parentPage = $parentPage;

        return $this;
    }

    /**
     * Get parentPage
     *
     * @return Page
     */
    public function getParentPage()
    {
        return $this->parentPage;
    }

    function getParentId()
    {
        return $this->parentId;
    }

    function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pageBlocks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pageBlocks
     *
     * @param PageBlock $pageBlocks
     * @return Page
     */
    public function addPageBlock(PageBlock $pageBlocks)
    {
        $this->pageBlocks[] = $pageBlocks;

        return $this;
    }

    public function resetPageBlocks()
    {
        $this->pageBlocks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Remove pageBlocks
     *
     * @param PageBlock $pageBlocks
     */
    public function removePageBlock(PageBlock $pageBlocks)
    {
        $this->pageBlocks->removeElement($pageBlocks);
    }

    /**
     * Get pageBlocks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPageBlocks()
    {
        return $this->pageBlocks;
    }



    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    public function getRedirectWindow()
    {
        return $this->redirectWindow;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function getTeaser()
    {
        return $this->teaser;
    }

    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
    }

    public function setRedirectWindow($redirectWindow)
    {
        $this->redirectWindow = $redirectWindow;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;
    }

    public function getLvlPrefix()
    {
        return str_repeat('- ', $this->getLvl() - 1);
    }

    public function hasLeftBar()
    {
        if ('menu-left-center-right' == $this->getLayout() or 'menu-left-center' == $this->getLayout())
        {
            return true;
        }
    }

    public function hasRightBar()
    {
        if ('menu-left-center-right' == $this->getLayout() or 'menu-center-right' == $this->getLayout())
        {
            return true;
        }
    }

    function getConfigData() {
        return $this->configData;
    }

    function setConfigData($configData): void {
        $this->configData = $configData;
    }




    /**
     * Get the value of siteItemPage
     */ 
    public function getSiteItemPage()
    {
        return $this->siteItemPage;
    }

    /**
     * Set the value of siteItemPage
     *
     * @return  self
     */ 
    public function setSiteItemPage($siteItemPage)
    {
        $this->siteItemPage = $siteItemPage;

        return $this;
    }

    /**
     * Get the value of multisite
     */ 
    public function getMultisite()
    {
        return $this->multisite;
    }

    /**
     * Set the value of multisite
     *
     * @return  self
     */ 
    public function setMultisite($multisite)
    {
        $this->multisite = $multisite;

        return $this;
    }
}
