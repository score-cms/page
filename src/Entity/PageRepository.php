<?php


namespace Score\PageBundle\Entity;

use Score\BaseBundle\Services\BaseRepository;
use Score\BlockBundle\Entity\Block;

class PageRepository extends BaseRepository
{
    public function findPageBlocks($page)
    {

        $conn =  $this->getEntityManager()->getConnection();

        $sql = 'SELECT b.* FROM cms_page_block pb 
                LEFT JOIN cms_block b ON pb.block_id = b.id where pb.page_id = :page_id  ';
        $records = $conn->fetchAll($sql,array('page_id'=>$page->getId()));


        $list = array();
        foreach($records as $record)
        {
            $block = new Block();
            $this->updateEntityFromArray($block,$record);
            $list[] = $block;
        }

        return $list;

    }


}
