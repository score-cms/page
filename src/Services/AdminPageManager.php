<?php

namespace Score\PageBundle\Services;

use Score\BlockBundle\Entity\Block;
use Doctrine\ORM\EntityManagerInterface;
use Score\CmsBundle\Entity\Multisite\SiteItemPage;
use Score\BaseBundle\Services\BaseManager as BaseManager;

class AdminPageManager extends BaseManager {

    private $keywordManager;
    private $treeManager;
    private $blockManager;

    function __construct($em,  $repository , $keywordManager, $treeManager, $blockManager)
    {
        $this->db_provider = $em;
        $this->repositiory = $this->db_provider->getManager()->getRepository($repository);
        $this->keywordManager = $keywordManager;
        $this->treeManager = $treeManager;
        $this->blockManager = $blockManager;
    }

    public function handleMultisite($page)
    {
        //remove old
        $repository = $this->db_provider->getManager()->getRepository('ScoreCmsBundle:Multisite\SiteItemPage');
        $qd = $repository->createQueryBuilder('t')->delete();
        $qd->where('t.page = :itemId');
        $qd->setParameter('itemId', $page->getId());
        $query = $qd->getQuery();
        $query->getResult();
        
        if(null != $page->getMultisite())
        {
            $multisites = [];
            foreach($page->getMultisite() as $site)
            {
                $siteItemPage = new SiteItemPage();
                $siteItemPage->setSite($site);
                $siteItemPage->setPage($page);
                $multisites[] = $siteItemPage; 
            }

            $page->setSiteItemPage($multisites);
        }
        else
        {
            $page->setSiteItemPage([]);
        }
    }

    public function getKeywordManager()
    {
        return $this->keywordManager;
    }

    public function setKeywordManager($keywordManager)
    {
        $this->keywordManager = $keywordManager;
    }

    public function getBlockManager()
    {
        return $this->blockManager;
    }

    public function setBlockManager($blockManager)
    {
        $this->blockManager = $blockManager;
    }

    public function getTreeManager()
    {
        return $this->treeManager;
    }

    public function setTreeManager($treeManager)
    {
        $this->treeManager = $treeManager;
    }

    public function getPagesByParent($parent)
    {
        return $this->getRepository()->findBy(
                        array('parentPage' => $parent), array('sortOrder' => 'ASC')
        );
    }

    public function getPageParentChoices($parentId = 1)
    {
        $repository = $this->getRepository();
        //$items = $repository->findAll(array('lvl' => 'ASC'));


        $items = $repository->createQueryBuilder('a')
                        ->select('a')
                        ->addOrderBy('a.lvl', 'ASC')
                        ->getQuery()->getResult();


        $treeManager = $this->getTreeManager();
        $treeManager->setCollection($items);
        $scalar = $treeManager->buildPageParentChoices($parentId);
        return $scalar;
    }

    public function getPageById($id)
    {
        return $this->getRepository()->find($id);
    }

    public function reorderPages($sortList)
    {
        $repo = $this->getRepository();
        $em = $this->getDbProvider()->getManager();
        foreach ($sortList as $sort => $pageId)
        {
            $sort++;
            $page = $repo->findOneBy(array('id' => $pageId));
            $page->setSortOrder($sort);
            $em->persist($page);
        }
        $em->flush();


    }

    public function getAvailableBlocks($page)
    {
        $manager = $this->getBlockManager();
        $groupEnum = $manager->getGroups();

        $em = $this->getDbProvider();


        $entities = $em->getRepository(Block::class)->loadBlocksList();
//        $entities = $em->getRepository('ScoreBlockBundle:Block')->findAll();
        $groups = [];
        foreach ($entities as $block)
        {
                $groups[$block->getType()]['blocks'][$block->getId()] = $block;
                $groups[$block->getType()]['name'] = $groupEnum[$block->getType()];
//                ksort($groups[$block->getType()]['blocks']);

        }

        return $groups;
    }

    /**
     * Returns available keywords for current page
     */
    public function getAvailableKeywords()
    {
        $keywords = $this->keywordManager->getAvailableKeywords();
        return $keywords;
    }

    public function getPageKeywords($page)
    {
        $keywords = $this->keywordManager->getPageKeywords($page);
        return $keywords;
    }

}
