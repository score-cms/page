<?php

namespace Score\PageBundle\Services;

class PageKeywordManager
{
        private $keywordManager;

        public function __construct($keywordManager)
	{
            $this->keywordManager = $keywordManager;
	}

	public function __destruct()
	{
	}

        public function getKeywordManager()
        {
            return $this->keywordManager;
        }

        public function getPageKeywords($page)
        {
             return $this->getKeywordManager()->getItemKeywords($page);
        }

	/**
	 *
	 * @param page
	 * @param keywords
	 */
	public function createPageKeywords($page,$keywords)
	{
            foreach($keywords->getItems() as $item)
            {
                $this->getKeywordManager()->createKeywordItem($item);
            }

        }

	/**
	 *
	 * @param page
	 */
	public function deletePageKeywords($page)
	{
            $this->getKeywordManager()->deleteKeywordsItem(array('itemId' => $page->getId(),'itemType' => 'page'));
	}


        /**
         * Return all available keywords
         */
        public function getAvailableKeywords()
        {
            $list = $this->getKeywordManager()->getKeywords();
            return $list;
        }

}
?>
