<?php

namespace Score\PageBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator as Paginator;

class PageDatagrid {

    public function getCols() {
  return array(

            '0' => 'a.name'
        );
}

    public function buildDataAsArray($paginator)
    {
        $data = array();
        foreach ($paginator as $object)
        {
            $data[] = array(
                '<a href="'.$this->getRouter()->generate('page_admin_edit',array('id' => $object->getId())).'">' . $object->getName() . '</a>',
            );
        }
        return $data;
    }


}
