# score/PageBundle #

SAZP CMS for Enviroportal

_This bundle is only one part of the Score CMS._

[tutorial for installing complete Score CMS](https://bitbucket.org/score-cms/score/src/2021.7/README.md "README.md")

### What is this repository for? ###

* Part of Score CSM
* Version: __2021.11__

### How do I get set up? ###

* add repository to `composer.json`

    ``` 
        "repositories": [
            {
                "type": "vcs",
                "url": "git@bitbucket.org:score-cms/page.git"
            }  
        ]
    ```


* Install score/PageBundle
    ```
    composer require score/page:"2021.11.*-dev"
    ```

* add routes to `config/routes/annotations.yaml`
  ```
  score_page:
      resource: "@ScorePageBundle/Controller/"
      prefix: /
      type: annotation
  ```


### Author ###
* Marek Hubáček
#### Coworker ####
* Jozef Hort

